/***banner carousel** */

$(document).ready(function () {
  var bgBannerHeight = $(".banner-background").height() + 24;
  $(".main-banner").height(bgBannerHeight);
});
$(".banner-slider").owlCarousel({
  loop: true,
  nav: true,
  margin: 0,
  dots: true,
  navText: [
    '<div class ="owl-prev"><i class="fa-solid fa-chevron-left fa-3x"></i></div>',
    '<div class ="owl-next"><i class="fa-solid fa-chevron-right fa-3x"></i></div>',
  ],
  items: 1,
  mouseDrag: false,
  animateIn: "fadeIn",
  animateOut: "fadeOut",
  autoplay: true,
  autoplayHoverPause: true,
});

/****scroll animation****/
//change icon when scroll 
//stick button back to to top
//stick nav bar
$(document).ready(function () {
  $(window).scroll(function () {
    if ($(document).scrollTop() > 400) {
      $(".nav-header").addClass("sticky");
      $("#btnScrollToTop").addClass("sticky");
      $(".nav-img-logo").attr("src", "assets/images/luna-logo-r.png");
    } else {
      $(".nav-header").removeClass("sticky");
      $("#btnScrollToTop").removeClass("sticky");
      $(".nav-img-logo").attr("src", "assets/images/luna-logo.png");
    }
  });
});
//animation when click to button
const btnScrollToTop = document.querySelector("#btnScrollToTop");
btnScrollToTop.addEventListener("click", function () {
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: "smooth",
  });
});
//change nav link of sectio qhen scroll
const navLink = document.querySelectorAll(".nav-link");
const sections = document.querySelectorAll("section");
function activeMenu() {
  let sectionCount = sections.length;
  while (
    --sectionCount &&
    window.scrollY + 65 < sections[sectionCount].offsetTop
  ) {}
  navLink.forEach((ltx) => ltx.classList.remove("active"));
  navLink[sectionCount].classList.add("active");

  //   sections[sectionCount].classList.add("active");
}
activeMenu();
window.addEventListener("scroll", activeMenu);

// const observer = new IntersectionObserver((entries) => {
//   entries.forEach((entry) => {
//     if (entry.isIntersecting) {
//       entry.target.classList.add("active");
//     }
//   });
// });
// sections.forEach((el) => observer.observe(el));

/*****add accordion to footer when screen is small *****/
$(document).ready(function () {
  var windowWidth = $(window).width();
  if (windowWidth <= 768) {
    $(".footer-title ").addClass("accordion ");
    var acc = document.getElementsByClassName("accordion");

    for (var i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }
  } else $(".footer-title ").removeClass("accordion ");
});

/***when click on nav link get element under navbar****/
var navbarHeight = $("#navbar").height() + 28;
$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function (event) {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        event.preventDefault();
        window.scrollTo({
          top: target.offset().top - navbarHeight,
          left: 0,
        });
      }
    }
  });
  /***collapse menu when select section*** */
$(".nav-link").on("click", function () {
  $(".navbar-collapse").collapse("hide");
});

/****spot cursor*** */
const cursor = document.querySelector("#cursor")
window.addEventListener("mousemove", (e) => {
  cursor.style.left = (e.x - 80 )+ 'px'
  cursor.style.top = (e.y - 280 )+ 'px'
});
